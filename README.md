# IDS 721 Mini Proj 6 [![pipeline status](https://gitlab.com/jaxonyue/ids-721-mini-proj-6/badges/main/pipeline.svg)](https://gitlab.com/jaxonyue/ids-721-mini-proj-6/-/commits/main)

## Overview
* This repository includes the components for **Mini-Project 6 - Instrument a Rust Lambda Function with Logging and Tracing**

## Goal
* Add logging to a Rust Lambda function
* Integrate AWS X-Ray tracing
* Connect logs/traces to CloudWatch

## My Lambda Function
* I used my Lambda function from week 2. My script is written in Rust and uses the Cargo Lambda framework. It contains two functions:
  * `uppercase` - This function takes a string as input and returns the string in uppercase
  * `lowercase` - This function takes a string as input and returns the string in lowercase

## Key Steps
1. Install Rust and Cargo Lambda per the instructions in the [Cargo Lambda documentation](https://www.cargo-lambda.info/guide/installation.html) and [Rust documentation](https://www.rust-lang.org/tools/install)
2. Create a new Rust project using `cargo lambda new <project_name>`
3. Write the code for the Lambda Function in the `src/main.rs` file
4. Test the Lambda Function by first `cargo lambda watch` to build the project and then `cargo lambda invoke <function_name> --event <event_file>.json` to test the function
5. Sign into AWS and go to the IAM console, add a new user, choose "Attch policies directly" and select "IAMFullAccess" and "AWSLambda_FullAccess"
6. Finish creating the user, navigate to the "Security Credentials" tab, and create an access key
7. Store AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY, and AWS_REGION in a newly created `.env` file
8. Create a `.gitignore` file and add `.env` to it to prevent the file from being pushed to Gitlab
9. In the terminal, run the following commands to set the environment variables
```
set -a # automatically export all variables
source .env
set +a
```
10. Run `cargo lambda deploy` to deploy the Lambda Function to AWS
11. Go to the AWS Lambda console and test the function by creating a new test event using the follwing event JSON:
```
{
  "command": "<insert command>"
  "data": "<insert data>"
}
```
12. Add a new trigger to the Lambda Function by selecting "API Gateway", creating a new API, REST API, and "Open" security
13. Deploy the API and test the function using the API Gateway URL using the following format:
```
curl -X POST -H "Content-Type: application/json" -d '{"command": "<insert command>", "data": "<insert data>"}' <API Gateway URL>
```
14. Now you should have a working Lambda Function that can be called using an API Gateway URL. My API Gateway URL is `https://l6b037e7k5.execute-api.us-east-1.amazonaws.com/default`
15. Add `AWS_ACCESS_KEY_ID`, `AWS_SECRET_ACCESS_KEY`, and `AWS_REGION` to the Gitlab CI/CD settings as environment variables
16. Copy my `.gitlab-ci.yml` file into the repository
17. Enable CloudWatch Logs and X-Ray Tracing in the Lambda Function settings
18. Go to IAM and add a role with `CloudWatchLogsFullAccess`

## AWS X-Ray Tracing Screenshot
![Screenshot_2024-03-07_at_2.55.02_PM](/uploads/cb60936643fd16c71f6d292a3c02ba4d/Screenshot_2024-03-07_at_2.55.02_PM.png)

## CloudWatch Logs Screenshot
![Screenshot_2024-03-07_at_2.26.16_PM](/uploads/12e08712d04b396eef4de5218e8c653d/Screenshot_2024-03-07_at_2.26.16_PM.png)